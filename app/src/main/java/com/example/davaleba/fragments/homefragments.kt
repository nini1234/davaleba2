package com.example.davaleba.fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.davaleba.R

class homefragments : Fragment(R.layout.fragment_home) {

    private lateinit var editTextAmount: EditText
    private lateinit var button: Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editTextAmount = view.findViewById(R.id.editTextAmount)
        button = view.findViewById(R.id.button)

        val controller = Navigation.findNavController(view)

        button.setOnClickListener{
            val amountText = editTextAmount.text.toString()

            if (amountText.isNotEmpty()){
                return@setOnClickListener
            }

            val amount = amountText.toInt()

            val action = homefragmentsDirections.actionHomefragments2ToDashboardFragment4(amount)

            controller.navigate(action)

        }

    }

}